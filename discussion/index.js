function displayMsgToSelf(){

		console.log("Dont't text her back.");	
};

let count = 10;

while(count !== 0){
	displayMsgToSelf();
	count--;
};

//While Loop - it will allow us to repeat an action/ instruction as long as the condition is true.

/*
		count = 10

		1st loop - count 10
		2nd loop - count 9
		3rd loop - count 8
		4th loop - count 7
		5th loop - count 6
		6th loop - count 5
		7th loop - count 4
		8th loop - count 3
		9th loop - count 2
		10th loop- count 1

		while loop checked if count is still not equal to zero:
			at this point, before a possible 11th loop count was decremeneted to 0. Therefore, there was no 11th loop.

		If there is no decrementation, the condition is always be true, thus, an infinite loop.

		Infinite loops will run our code block forever until you stop it.
*/
let num = 1;

while(num <= 5){
	console.log(num);
	num++;
};

//Do-while Loop

	/*
		Do-while loop is similar to the while loop. However, the Do-while loop will allow us to run our loop at least once.

		With the while loop we check the condition first before running our code block, however, for the Do-while loop, it will do an instruction first before it will check the condition to run again.
	*/

let doWhileCounter = 1;

do {
	console.log(doWhileCounter);
} while(doWhileCounter === 0){
	console.log(doWhileCounter);
	doWhileCounter--;
};

/*
	Mini-activity
		create a do-while loop which will be able to show the numbers in the console from 1-20 in order.

*/

let x = 1;

do {
	console.log(x);
	x++;
}while (x <= 20);


// For loop
	/*
		for loops are much more flexible than the while, and do-while loops. It consists of three parts: 

				1. Initialization - creating a variable which can be used as the counter
				2. Condition - creating the appropriate condition to run our loop
				3. finalExpression or (decrementation or incrementation) based on your condition.

			Syntax: 
					for (initialization; expression/condition; final expression (++/--)){
							code block
					};
	*/

	for(let count = 0; count <= 20; count++){
		console.log(count);
	};

	/*Accessing Array Items
			- Each item is an array is ordered accordingly.
			- Each item is orderd per index
			- Each array starts their index at 0

	*/

let fruits = ["Apple", "Durian", "Kiwi", "Pineapple", "Strawberry"];

//We can access the items in an array by accessing them through their index.
//This will allow us to see the item in our array.
console.log(fruits[2]);
console.log(fruits[1]);

/*
	Mini-Activity

	Show the value of the first and last items in the fruits array.
*/

console.log(fruits[0]);
console.log(fruits[4]);

//.length property is also a property of an array. The function of .length property in an array shows the total number of items in an array

console.log(fruits.length);//5 - total number of items in an array.


//A more reliable way of checking the last item in an array
//arrayName[arrayName.length-1]
console.log(fruits[fruits.length-1]);//strawberry

//show all the items in an array in the comsole using a for loop

for(let index=0; index < fruits.length; index++){
	console.log(fruits[index]);
};

/*
		Mini-Activity

			Create an array with at least 6 items
				- Each item as one of your favorite countries
			Display all items in the console except for the last item.

*/

let countries = ["Italy", "Switzerland", "United Kingdom", "France", "Singapore", "Kenya"];

for(let index=0; index < countries.length-1; index++){
	console.log(countries[index]);
};

//if we assign a new value to an element - we use the assignment operator (=)

fruits[5] = "Grapes";
console.log(fruits);

//Example of Array of objects

let cars = [
	{
		brand: "Toyota",
		type: "Sedan"
	},
	{
		brand: "Rolls Royce",
		type: "Luxury Sedan"
	},
	{
		brand: "Mazda",
		type: "Hatchback"
	}
];
console.log(cars.length);//3 items

console.log(cars[1]);

let myName = "adrIAn madArang";

/*
	Mini-Activity

	1. Create a loop that will print out the letter of the name individually and print out the number 3 instead when the letter to be printed out is a vowel.
		-How this for loop works:
			1. The loop will start at 0 for the value of 'i'
			2. It will check if 'i' is less than the length of myName (e.g. 0)
			3. The if statement will check if the value of myName[i] converted to a lowercase letter is equivalent to any of the vowels (e.g. myName[0] = a, myName[0]=e, myName[0]=i, myName[0]=o, myName[0]=u)
			4. If the expression/condition is true the console will print the letter
			5. If the letter is not a vowel the console will print the letter
			6. The value of 'i' will be incremented by 1
			7. Then the loop will repeat from step 2 to 6 until the expression/condition of the loop is false.
*/

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 

	) {
		console.log(3);
	}else {
		console.log(myName[i]);
	}
};

// continue and break
	//The continue statement allows the code to go to the next iteration of the loop without finishing the execution of the following statement in code block
	//skips the current loop and proceed to the next loop


for(let a = 20; a > 0; a--){
	/*if(a % 2 === 0){
		continue
	}*/

	if(a < 10){
		break
	}
	console.log(a);
};

let string = "alejandro";

for(let i = 0; i < string.length; i++){
	if(string[i].toLowerCase() === "a"){
		console.log("continue to the next iteration")
		continue;
	}

	if(string[i].toLowerCase() == "d"){
		break;
	}
};