//First Solution

// We can also use the keyword "Number" to parse the number as a "number" and not a string. 
// e.x. let num = Number(prompt("Enter a number: "));
/*for(let num = parseInt(prompt("Enter a number: ")); num >= 0; num--){

	if(num <= 50){
		break;
	}else if(num % 10 == 0){
		console.log("The number is divisible by 10. Skipping the number");
		continue;
	}else if(num % 5 == 0){
		console.log(`${num}`);
	}

	
};*/

//decrement = to decrease a value by 1 on each iteration
let num = Number(prompt("Enter a number: "));
console.log(typeof num);

for(let count = num; count >= 0; count--){
	
	if(count <= 50){
		console.log("The current value is at " + count + ". Terminating the loop.");
		break;
	} else if(count % 10 === 0){
		console.log(" The current value is at " + count + ". Skipping the number.");
		continue;

	}else if(count % 5 === 0){
		console.log(count);

	}

};

// Get consonants from a string
const Consonants = (str) => {
    const vowels = ['a', 'e', 'i', 'o', 'u'];
    // Check for vowels

    let letters = str.split('');
    let vowelsFound = [], consonantsFound = [];

    for (let i in letters) {
        if (vowels.includes(letters[i])) {
            vowelsFound.push(letters[i]);
        } else {
            consonantsFound.push(letters[i]);
        }
    }
    
    console.log(str);
    console.log(consonantsFound.join(""));    
}

var str = "supercalifragilisticexpialidocious";
Consonants(str);